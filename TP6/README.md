# Trabajo Práctico 6

El trabajo práctico 6 se encuentra resuelto [aquí](./DSS2020-TP6-Bianchini.pdf)

## Stack docker
El [stack docker](./docker-compose.yml) del trabajo práctico 6 cuenta con los siguientes servicios:
- jupyter: Expuesto en el puerto 8888 y utilizado para resolver todos los puntos del trabajo.

## Ejecución del stack:
1. Dentro del directorio [Servicios](./Servicios) se encuentran los directorios que contienen los archivos de interés para cada servicio. Dentro de ellos, se encuentran archivos de ambiente .env.template ([jupyter](./Servicios/jupyter/.env.template)) que contienen variables necesarias para la ejecución del stack. Se debe ingresar a cada uno de esos directorios para generar un .env para cada servicio con las variables correspondientes.
2. Una vez generados todos los archivos .env, ejecutar el comando ```docker-compose up -d``` en el mismo directorio del archivo ```docker-compose.yml``` para levantar el stack.
3. Con el navegador, ir a la dirección ```localhost:8888``` (Contraseña: ```dss``` si se utiliza el .env por defecto) para entrar al notebook
4. Una vez dentro del notebook se visualizarán los siguientes archivos notebook con las soluciones de cada punto:
    - [Preprocesamiento de Datos.ipynb](./Resoluciones/1.\ Preprocesamiento\ de\ Datos.ipynb)
    - [Regresión.ipynb](./Resoluciones/2.\ Regresión.ipynb)
    - [Clasificación.ipynb](./Resoluciones/3.\ Clasificación.ipynb)
    - [Clustering.ipynb](./Resoluciones/4.\ Clustering.ipynb)
    - [Métodos de Ensambles.ipynb](./Resoluciones/5.\ Métodos\ de\ Ensambles.ipynb)
    - [Cuantificación de la calidad de las predicciones.ipynb](./Resoluciones/6.\ Cuantificación\ de\ la\ Calidad\ de\ las\ Predicciones.ipynb)
