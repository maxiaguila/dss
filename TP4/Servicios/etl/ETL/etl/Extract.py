import sys
import json
import pandas as pd
sys.path.append("..")
from utils.Constantes import COLUMNAS_COVID, COVID_TABLE_NAME, TIME_TABLE_NAME, PROV_TABLE_NAME, TEST_TABLE_NAME


class Extract:
    def extract_covid_data(self, covid_file_name):
        return pd.read_csv(covid_file_name, usecols=COLUMNAS_COVID.keys())

    def extract_provs_data(self, provs_file_name):
        with open(provs_file_name, 'r') as provs_file:
            return json.load(provs_file)
        return []

    def execute(self, covid_file_name, provs_file_name):
        return self.extract_covid_data(covid_file_name), self.extract_provs_data(provs_file_name)
