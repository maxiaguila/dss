import hashlib
from sqlalchemy import Column, Integer, String
import sys
sys.path.append("..")
from utils.Constantes import Base, PROCESSED_FILE_TABLE_NAME


class ProcessedFile(Base):
    __tablename__ = PROCESSED_FILE_TABLE_NAME

    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    hash = Column(String)

    @staticmethod
    def calculate_hash(file_name):
        hash_md5 = hashlib.md5()
        with open(file_name, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()
