from sqlalchemy import Column, Integer
import sys
sys.path.append("..")
from utils.Constantes import Base, TIME_TABLE_NAME


class Tiempo(Base):
    __tablename__ = TIME_TABLE_NAME

    id = Column(Integer, primary_key=True)
    dia = Column(Integer)
    mes = Column(Integer)
    año = Column(Integer)
