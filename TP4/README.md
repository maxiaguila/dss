# Trabajo Práctico 4

## Stack
El stack docker del trabajo práctico 4 cuenta con los siguientes servicios:
- jupyter: (Utilizado para los puntos 1 y 2 de la sección de minería de datos y los puntos 1 y 2 de la sección de visualización de datos)
- mongodb: (Utilizado para el punto 2) 
- postgres: (Utilizado para la sección de visualizaciónd e datos del trabajo)

**NOTA: Se hereda el servicio de python (Con el sistema ETL desarrollado) del trabajo anterior para utilizar los datos en la sección de visualización de datos** 


## Ejecución del stack:
1. Ejecutar el comando ```docker-compose up -d``` en el mismo directorio del archivo ```docker-compose.yml``` para levantar el stack.
2. Ejecutar el ETL: ``` docker exec -it dss_tp4_etl python /etl/main.py```
3. Con el navegador, ir a la dirección ```localhost:8888``` (Contraseña: ```dss```) para entrar al notebook
4. Una vez dentro del notebook se visualizará la siguiente jerarquía de carpetas:
    - Minería de Datos
        - Punto 1
        - Punto 2
    - Visualizacion de Datos
        - Punto 1
        - Punto 2

## Sección: Minería de Datos
### Punto 1:
Explicación resolucion punto 1
### Punto 2:
Explicación resolucion punto 2
### Punto 3:
Explicacion resolucion punto 3

## Sección: Visualización de Datos
### Punto 1:
Explicación resolucion punto 1
### Punto 2:
Explicación resolucion punto 2



