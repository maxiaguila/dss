------------------------------------------------------
---------------------TABLESPACE-----------------------
------------------------------------------------------
-- Creamos TableSpace de la DB1
CREATE TABLESPACE ts1 LOCATION '/run/postgresql/ts/db1';

-- Creamos TableSpace de la DB2
CREATE TABLESPACE ts2 LOCATION '/run/postgresql/ts/db2';

------------------------------------------------------
------------------WINDOW FUNCTION---------------------
------------------------------------------------------

-- Creo la tabla productos
-- DROP TABLE products
CREATE TABLE products (
	id serial primary key,
	name varchar not null,
	brand varchar not null,
	price float not null
);

-- Inserto productos a la tabla
insert into products (name, brand, price) values
	('product-1', 'brand-A', 100),
	('product-2', 'brand-B', 90.25),
	('product-3', 'brand-C', 94.50),
	('product-4', 'brand-A', 113),
	('product-5', 'brand-B', 124.50),
	('product-6', 'brand-C', 87.75),
	('product-7', 'brand-A', 105),
	('product-8', 'brand-A', 103.50),
	('product-9', 'brand-C', 110)

select * from products;

-- Creo la Window Function particionando por "brand"
select *, avg(price) over (partition by brand) from products;