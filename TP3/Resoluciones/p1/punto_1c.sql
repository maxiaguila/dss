
-- DROP TABLE time;
CREATE TABLE time(
	id serial primary key,
	day integer check(day>=1 and day <=31) not null,
	month integer check(month>=1 and month<=12) not null,
	year integer not null,
	hour integer check(hour>=0 and hour<=23)not null,
	minutes integer check(minutes>=0 and minutes<=59) not null
);

-- DROP TABLE passenger;
CREATE TABLE passenger (
	id serial primary key,
	name varchar not null,
	surname varchar not null,
	address varchar not null,
	city varchar not null,
	phone varchar not null,
	email varchar not null,
	country varchar not null
);

-- DROP TABLE location;
CREATE TABLE location (
	id serial primary key,
	country varchar not null,
	province varchar not null,
	city varchar not null,
	postal_code integer check (postal_code>0)
);

-- DROP TABLE company;
CREATE TABLE company (
	id serial primary key,
	name varchar not null,
	code integer not null check (code>0),
	contact varchar not null
);

-- DROP TABLE airplane;
CREATE TABLE airplane (
	id serial primary key,
	id_company integer not null,
	capacity integer not null, -- en pasajeros
	modelo varchar not null,
	luxurity boolean not null,
	weight float not null,
	height float not null,
	width float not null,
	motor varchar not null,
	constraint fk_company foreign key (id_company) references company(id)
);

-- DROP TABLE airport;
CREATE TABLE airport (
	id serial primary key,
	id_location integer not null,
	code integer not null check (code>0),
	name varchar not null,
	international boolean not null,
	constraint fk_location foreign key (id_location) references location(id)
);

-- DROP TABLE flight;
CREATE TABLE flight (
	id serial primary key,
	id_origin_airport integer not null,
	id_destination_airport integer not null,
	id_airplane integer not null,
	departure_time varchar not null,
	arrival_time varchar not null,
	constraint fk_origin_airport foreign key (id_origin_airport) references airport(id),
	constraint fk_destination_airport foreign key (id_origin_airport) references airport(id),
	constraint fk_airplane foreign key (id_origin_airport) references airplane(id)
);

-- DROP TABLE reserve;
CREATE TABLE reserve (
	id serial primary key,
	id_flight integer not null,
	id_airport integer not null,
	id_time integer not null,
	id_passenger integer not null,
	amount float not null check (amount>0),
	is_payed boolean not null,
	spot_row integer not null check (spot_row>0),
	spot_letter integer not null check (spot_letter>0),
	spot_class varchar not null
);

-- Create times
insert into time (day, month, year, hour, minutes) values
	(15, 05, 2020, 15, 30),
	(28, 02, 2019, 16, 00),
	(19, 12, 2019, 10, 15),
	(08, 07, 2019, 12, 00),
	(09, 09, 2020, 17, 30),
	(01, 01, 2020, 18, 00),
	(17, 03, 2019, 16, 00),
	(31, 07, 2018, 16, 00),
	(11, 11, 2017, 17, 30),
	(24, 04, 2018, 12, 15),
	(19, 05, 2017, 10, 30),
	(27, 10, 2016, 10, 00),
	(18, 07, 2016, 13, 45),
	(02, 10, 2015, 13, 00),
	(19, 11, 2015, 18, 00),
	(17, 01, 2018, 09, 00),
	(01, 10, 2017, 11, 30),
	(31, 07, 2016, 10, 30),
	(14, 10, 2018, 09, 30),
	(05, 11, 2018, 13, 45),
	(30, 10, 2011, 10, 30),
	(28, 02, 2020, 09, 30),
	(24, 05, 2018, 19, 30),
	(01, 03, 2020, 10, 30),
	(30, 01, 2020, 09, 30),
	(11, 03, 2018, 02, 00),
	(24, 04, 2018, 20, 15);
	
-- create passenger
insert into passenger (name, surname, address, city, phone, email, country) values
	('Tony', 'Stark', 'calle falsa 123', 'Trelew', '1234567890', 'tonystark@gmail.com', 'Argentina'),
	('Walter', 'White', 'Piermont Drive3828', 'Madryn', '0987654321', 'heisenberg@gmail.com', 'Argentina'),
	('Lucifer', 'Morningstar', 'San Martin 666, Penthouse lux', 'Madryn', '1234554321', 'lucifer@gmail.com', 'Argentina'),
	('Bruce', 'Waine', 'Avenida Corrientes 999', 'Rawson', '6789012345', 'iambatman@gmail.com', 'Argentina'),
	('Arthur', 'Shelby', 'M.Jones 246', 'Trelew', '1221122112', 'byorderofthepeakyblinders@gmail.com', 'Argentina');

-- create locations
insert into location (country, province, city, postal_code) values
	('Argentina', 'Cordoba', 'Carlos Paz', 1234),
	('Argentina', 'Mendoza', 'Mendoza', 4321),
	('Argentina', 'Buenos Aires', 'Buenos Aires', 1234),
	('España', 'Andalucia', 'Sevilla', 9999),
	('España', 'Extremadura', 'Caceres', 1010),
	('Canada', 'Ontario', 'Toronto', 1111),
	('Argentina', 'Misiones', 'Posadas', 2222);
	
-- create company
insert into company (name, code, contact) values
	('company-1', 000001, 'contact-1'),
	('company-2', 000002, 'contact-2'),
	('company-3', 000003, 'contact-3'),
	('company-4', 000004, 'contact-4'),
	('company-5', 000005, 'contact-5');
	
-- create airplanes
insert into airplane (id_company, capacity, modelo, luxurity, weight, height, width, motor) values
	(1, 400, 'model-1', false, 60, 5, 55, 'motor-1'),
	(2, 450, 'model-2', true, 70, 6, 60, 'motor-2'),
	(3, 600, 'model-3', false, 60, 5, 50, 'motor-3'),
	(4, 700, 'model-4', true, 65, 4.5, 59, 'motor-4'),
	(5, 350, 'model-5', false, 60, 5, 55, 'motor-5');

-- create airport
insert into airport (id_location, code, name, international) values
	(1, 001, 'name-1', false),
	(2, 002, 'name-2', false),
	(3, 003, 'name-3', true),
	(4, 004, 'name-4', true),
	(5, 005, 'name-5', false);

-- create flights
insert into flight (id_origin_airport, id_destination_airport, id_airplane,	departure_time, arrival_time) values
	(1, 2, 1, '15-05-2020 15:30', '15-05-2020 14:30'),
	(3, 4, 2, '28-02-2019 16:00', '16-05-2020 14:00'),
	(2, 6, 3, '19-12-2020 15:30', '19-12-2020 16:30'),
	(2, 6, 4, '08-05-2018 15:30', '09-05-2018 14:30'),
	(1, 2, 1, '15-05-2018 15:30', '15-05-2018 16:30'),
	(1, 2, 5, '23-07-2018 15:30', '23-07-2018 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 2, 3, '15-05-2020 15:30', '15-05-2020 16:30'),
	(1, 2, 1, '15-05-2020 15:30', '15-05-2020 14:30'),
	(3, 4, 2, '28-02-2019 16:00', '16-05-2020 14:00'),
	(2, 6, 3, '19-12-2020 15:30', '19-12-2020 16:30'),
	(2, 6, 4, '08-05-2018 15:30', '09-05-2018 14:30'),
	(1, 2, 1, '15-05-2018 15:30', '15-05-2018 16:30'),
	(1, 2, 5, '23-07-2018 15:30', '23-07-2018 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 5, 2, '15-05-2019 15:30', '16-05-2019 14:30'),
	(3, 2, 3, '15-05-2020 15:30', '15-05-2020 16:30');

-- create reserve 
insert into reserve (id_flight,	id_airport, id_time, id_passenger, amount, is_payed, spot_row, spot_letter, spot_class) values
	(2, 1, 5, 1, 70000, true, 5, 4, 'class-1'),
	(3, 1, 1, 1, 95000, true, 1, 4, 'class-1'),
	(4, 1, 1, 1, 44000, true, 5, 4, 'class-1'),
	(2, 1, 1, 1, 31000, true, 1, 4, 'class-1'),
	(1, 1, 1, 1, 4000, true, 25, 4, 'class-1'),
	(2, 3, 2, 2, 20000, true, 45, 6, 'class-2'),
	(3, 2, 3, 3, 4500, true, 13, 3, 'class-1'),
	(2, 3, 2, 4, 20000, true, 27, 5, 'class-2'),
	(8, 3, 5, 5, 4200, true, 16, 4, 'class-1'),
	(4, 2, 6, 6, 18000, true, 25, 4, 'class-2'),
	(1, 1, 1, 1, 4000, true, 33, 2, 'class-2'),
	(9, 1, 13, 1, 70000, true, 5, 4, 'class-1'),
	(10, 1, 14, 1, 70000, true, 5, 4, 'class-1'),
	(11, 1, 15, 1, 70000, true, 5, 4, 'class-1'),
	(12, 1, 16, 1, 70000, true, 5, 4, 'class-1'),
	(13, 1, 17, 1, 70000, true, 5, 4, 'class-1'),
	(14, 1, 18, 1, 70000, true, 5, 4, 'class-1'),
	(15, 1, 19, 1, 70000, true, 5, 4, 'class-1'),
	(16, 1, 20, 1, 70000, true, 5, 4, 'class-1'),
	(17, 1, 21, 1, 70000, true, 5, 4, 'class-1'),
	(18, 1, 22, 1, 70000, true, 5, 4, 'class-1'),
	(19, 1, 23, 1, 70000, true, 5, 4, 'class-1'),
	(20, 1, 24, 1, 70000, true, 5, 4, 'class-1'),
	(21, 1, 25, 1, 70000, true, 5, 4, 'class-1'),
	(22, 1, 26, 1, 70000, true, 5, 4, 'class-1'),
	(23, 1, 27, 1, 70000, true, 5, 4, 'class-1');


/**
 * Consultas
 */
-- ¿Cuál es el destino más visitado por los pasajeros de la ciudad de Trelew en los meses del año 2019?
select l.city as location_city, t.month as month, count(t.month) as qty
from reserve r
join time t on r.id_time = t.id
join passenger p on r.id_passenger = p.id
join flight f on r.id_flight = f.id
join airport a on f.id_destination_airport = a.id
join location l on a.id_location = l.id
where p.city = 'Trelew'
group by location_city, month
order by qty desc;

-- ¿Qué avión ha volado con mayor frecuencia con respecto a cada mes a lo largo de un periodo de 3 años?
select a.modelo modelo_avion, t.month as mes, count(t.month) as cantidad_viajes
from reserve r
join time t on r.id_time = t.id
join flight f on r.id_flight = f.id
join airplane a on f.id_airplane = a.id
where t.year >= '2017'
group by modelo_avion, mes
order by cantidad_viajes desc;

-- ¿Cuál fue la cantidad de pasajeros promedio para cada tipo de avión durante 2018?
select a.modelo tipo_avion, count(a.modelo) as cantidad_pasajeros 
from reserve r
join time t on r.id_time = t.id
join flight f on r.id_flight = f.id
join airplane a on f.id_airplane = a.id
where t.year = '2018'
group by tipo_avion
order by cantidad_pasajeros desc;
