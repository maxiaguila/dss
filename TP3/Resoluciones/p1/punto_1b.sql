/**
 * Creación del DW y carga de datos
 */
-- Database: tp3_p1_inmobiliaria

-- DROP DATABASE tp3_p1_inmobiliaria;

CREATE DATABASE tp3_p1_inmobiliaria
    WITH 
    OWNER = dss
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
-- DROP TABLE time;
CREATE TABLE time(
	id serial primary key,
	day integer check(day>=1 and day <=31) not null,
	month integer check(month>=1 and month<=12) not null,
	year integer not null,
	hour integer check(hour>=0 and hour<=23)not null,
	minutes integer check(minutes>=0 and minutes<=59) not null
);

-- DROP TABLE agent;
CREATE TABLE agent (
	id serial primary key,
	name varchar not null,
	surname varchar not null,
	office varchar not null,
	address varchar not null,
	city varchar not null,
	phone varchar not null
);

-- DROP TABLE estate;
CREATE TABLE estate (
	id serial primary key,
	ownerName varchar not null,
	category varchar not null,
	area varchar not null,
	city varchar not null,
	province varchar not null,
	rooms integer not null,
	bedrooms integer not null,
	garage boolean not null,
	meters integer not null
);
-- DROP TABLE agent;
CREATE TABLE agenda (
	id serial primary key,
	id_agent integer not null,
	id_time integer not null,
	id_estate integer not null,
	data varchar not null,
	clientName varchar not null,
	isSale boolean not null,
	isRent boolean not null,
	visitDuration integer not null, -- En minutos
	price float not null,
	constraint fk_estate foreign key (id_estate) references estate(id),
	constraint fk_agent foreign key (id_agent) references agent(id),
	constraint fk_time foreign key (id_time) references time(id)
);

-- Carga de Datos
-- Create times
insert into time (day, month, year, hour, minutes) values
	(15, 05, 2020, 15, 30),
	(28, 02, 2019, 16, 00),
	(19, 12, 2019, 10, 15),
	(08, 07, 2019, 12, 00),
	(09, 09, 2020, 17, 30),
	(01, 01, 2020, 18, 00),
	(17, 03, 2019, 16, 00),
	(31, 07, 2018, 16, 00),
	(11, 11, 2017, 17, 30),
	(24, 04, 2018, 12, 15),
	(19, 05, 2017, 10, 30),
	(27, 10, 2016, 10, 00),
	(18, 07, 2016, 13, 45),
	(02, 10, 2015, 13, 00),
	(19, 11, 2015, 18, 00);

-- create agents
insert into agent (name, surname, office, address, city, phone) values
	('Tony', 'Stark', 'office-1', 'calle falsa 123', 'Trelew', '1234567890'),
	('Walter', 'White', 'office-2', 'Piermont Drive3828', 'Trelew', '0987654321'),
	('Lucifer', 'Morningstar', 'office-1', 'San Martin 666, Penthouse lux', 'Madryn', '1234554321'),
	('Bruce', 'Waine', 'office-3', 'Avenida Corrientes 999', 'Rawson', '6789012345'),
	('Arthur', 'Shelby', 'office-4', 'M.Jones 246', 'Madryn', '1221122112');
	
-- create estates
insert into estate (ownerName, category, area, city, province, rooms, bedrooms, garage, meters) values
	('Thomas Shelby', 'category-1', 'North', 'Madryn', 'Chubut', 2, 1, true, 110),
	('Jhonny Deep', 'category-2', 'South', 'Trelew', 'Chubut', 2, 0, true, 95),
	('Bradley Cooper', 'category-3', 'East', 'Rawson', 'Chubut', 1, 1, false, 87),
	('Thomas Shelby', 'category-2', 'South', 'Madryn', 'Chubut', 3, 1, true, 125),
	('Jhonny Deep', 'category-1', 'North', 'Trelew', 'Chubut', 2, 1, true, 105),
	('Thomas Shelby', 'category-2', 'East', 'Trelew', 'Chubut', 2, 1, true, 110),
	('Jack Nicholson', 'category-1', 'South', 'Madryn', 'Chubut', 2, 0, true, 115),
	('Roger Federer', 'category-2', 'East', 'Trelew', 'Chubut', 3, 1, true, 120),
	('Jhonny Deep', 'category-3', 'East', 'Rawson', 'Chubut', 2, 1, true, 95);

-- create agenda
insert into agenda (id_agent, id_time, id_estate, data, clientName, isSale, isRent, visitDuration, price) values
	(1,1,1, 'data-1', 'name-1', true, false, 60, 2000000),
	(1,2,2, 'data-2', 'name-2', true, false, 45, 1500000),
	(2,3,3, 'data-3', 'name-3', true, false, 75, 1200000),
	(3,4,4, 'data-4', 'name-4', true, false, 60, 1800000),
	(1,5,5, 'data-5', 'name-5', true, false, 35, 2500000),
	(4,6,6, 'data-6', 'name-6', true, false, 55, 1950000), 
	(4,7,7, 'data-7', 'name-7', true, false, 65, 3000000),
	(5,8,8, 'data-8', 'name-8', true, false, 50, 1750000),
	(3,9,9, 'data-9', 'name-9', true, false, 45, 1250000);
	


/**
 * Consultas
 */

-- ¿Qué tipo de propiedad se vendió por el precio más alto con respecto a cada ciudad y meses?
select max(a.price) as price, e.category, e.city, t.month
from agenda a
join estate e on a.id_estate = e.id
join time t on a.id_time = t.id
where a.isSale = true
group by e.category, e.city, t.month;

-- ¿Quién ha comprado un piso con el precio más alto con respecto a cada mes?
select max(a.price) as max_price, t.month
from agenda a
  join estate e on a.id_estate = e.id
  join time t on a.id_time = t.id
where a.isSale = true
  and e.category = 'category-2'
group by month;

-- ¿Cuál es la duración media de visitas en las propiedades de cada categoría?
select sum(a.visitDuration) / count(a.visitDuration) as mean_duration, e.category
from agenda a
join estate e on a.id_estate = e.id
group by e.category
order by e.category;
