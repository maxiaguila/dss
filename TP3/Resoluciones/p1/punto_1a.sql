/**
 * Creación del DW y carga de datos
 */

-- DROP DATABASE tp1_vinos;
CREATE DATABASE tp3_p1_vinos
    WITH 
    OWNER = dss
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- DROP TABLE time;
CREATE TABLE time(
	id serial primary key,
	day integer not null,
	month integer not null,
	year integer not null
);

-- DROP TABLE customer;
CREATE TABLE customer(
	id serial primary key,
	code varchar not null,
	name varchar not null,
	address varchar not null,
	phone varchar not null,
	b_day date not null,
	gender varchar not null
);

-- DROP TABLE class;
CREATE TABLE class(
	id serial primary key,
	code varchar not null,
	name varchar not null,
	region varchar not null
);

-- DROP TABLE wine;
CREATE TABLE wine(
	id serial primary key,
	id_class integer not null,
	code varchar not null,
	name varchar not null,
	type varchar not null,
	vintage varchar not null,
	bottle_price float not null,
	case_price float null,
	constraint fk_class foreign key (id_class) references class(id)
);

-- DROP TABLE purchase_order;
CREATE TABLE purchase_order(
	id serial primary key,
	id_customer integer not null,
	id_wine integer not null,
	id_time integer not null,
	num_bottles integer not null,
	num_cases integer not null,
	constraint fk_customer foreign key (id_customer) references customer(id),
	constraint fk_wine foreign key (id_wine) references wine(id),
	constraint fk_time foreign key (id_time) references time(id)
);

-- Create customers
insert into customer (code, name, address, phone, b_day, gender) values 
	('123asd', 'Customer 1', 'Calle falsa 123', '0123456789', '1990-09-12 00:00:00', 'Hombre'),
	('123asf', 'Customer 2', 'Calle falsa 123', '9876543210', '1990-09-12 00:00:00', 'Mujer'),
	('123adf', 'Customer 3', 'Av Pueyrredon 2', '9876543210', '1990-09-12 00:00:00', 'Hombre'),
	('123sdf', 'Customer 4', 'Soberania Nacional 90', '9876543210', '1990-09-12 00:00:00', 'Mujer'),
	('124asd', 'Customer 5', 'Paraguay 943', '9876543210', '1990-09-12 00:00:00', 'Mujer');

-- Create classes
insert into class (code, name, region) values 
	('C000001', 'Clase 1', 'Region 1'),
	('C000002', 'Clase 2', 'Region 1'),
	('C000003', 'Clase 3', 'Region 1'),
	('C000004', 'Clase 4', 'Region 2'),
	('C000005', 'Clase 5', 'Region 3');

-- Create Wines
insert into wine (id_class, code, name, type, vintage, bottle_price, case_price) values
	(1, 'W000001', 'Wine 1', 'Type 1', 'Vintage 1', 500.0, 3000.0),
	(1, 'W000002', 'Wine 2', 'Type 1', 'Vintage 1', 550.0, 3300.0),
	(1, 'W000003', 'Wine 3', 'Type 2', 'Vintage 1', 679.0, 4074.0),
	(2, 'W000004', 'Wine 4', 'Type 1', 'Vintage 4', 1790.0, 10740.0),
	(2, 'W000005', 'Wine 5', 'Type 2', 'Vintage 2', 800.0, 4800.0),
	(2, 'W000006', 'Wine 6', 'Type 2', 'Vintage 2', 799.0, 4794.0),
	(3, 'W000007', 'Wine 7', 'Type 2', 'Vintage 3', 745.0, 4470.0),
	(4, 'W000008', 'Wine 8', 'Type 3', 'Vintage 3', 890.0, 5340.0),
	(5, 'W000009', 'Wine 9', 'Type 1', 'Vintage 4', 2597.0, 15582.0),
	(5, 'W000010', 'Wine 10', 'Type 2', 'Vintage 1', 700.0, 4200.0);

-- Create times
insert into time (day, month, year) values
	(15, 05, 2009),
	(28, 02, 2004),
	(19, 12, 1994),
	(08, 07, 2014),
	(09, 09, 2014),
	(01, 01, 2019),
	(17, 03, 2001),
	(31, 07, 2020),
	(11, 11, 2011),
	(24, 04, 2020),
	(19, 05, 2020),
	(27, 10, 2020),
	(18, 07, 2020),
	(02, 10, 2020),
	(19, 11, 2019);

-- Create purchase orders
insert into purchase_order (id_customer, id_wine, id_time, num_bottles, num_cases) values 
	(1, 1, 1, 5, 0),
	(2, 1, 5, 1, 0),
	(2, 5, 7, 6, 1),
	(3, 4, 8, 6, 2),
	(4, 7, 9, 3, 0),
	(5, 10, 10, 3, 0),
	(5, 10, 11, 5, 0),
	(3, 4, 12, 0, 5),
	(3, 1, 12, 15, 3),
	(1, 2, 13, 3, 0),
	(2, 6, 13, 2, 0),
	(2, 2, 13, 5, 1),
	(1, 6, 14, 0, 1),
	(2, 1, 15, 0, 1),
	(5, 10, 15, 1, 5);

/**
 * Consultas
 */

-- Muestre los porcentajes de tipos de vinos más vendidos en X año.
select type, qty, qty * 100 / sum(qty) over() as percentage
from (select w.type as type, count(w.type) as qty
	from purchase_order po
	join time t on po.id_time = t.id
	join wine w on po.id_wine = w.id
	where t.year = '2020'
	group by w.type
) as w2020;

-- ¿Cuál es la temporada con mayor cantidad de ventas de X vino?
select t.year, count(t.year)
from purchase_order po
join wine w on po.id_wine = w.id
join time t on po.id_time = t.id
where w.code = 'W000002'
group by t.year
order by t.year desc
limit 1;

-- ¿Qué clientes han realizado más compras a lo largo de 4 años?
select c.code, count(c.code) as qty
from purchase_order po
join customer c on po.id_customer = c.id
join time t on po.id_time = t.id
where t.year >= '2016' and t.year <= '2020'
group by c.code
order by qty desc;
