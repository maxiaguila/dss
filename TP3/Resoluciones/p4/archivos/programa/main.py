from etl.Etl import ETL
from utils.Constantes import COVID_FILE_NAME, PROVS_FILE_NAME


if __name__ == '__main__':
    etl = ETL(COVID_FILE_NAME, PROVS_FILE_NAME)
    etl.execute()
