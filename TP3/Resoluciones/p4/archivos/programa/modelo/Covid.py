from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
import sys
sys.path.append("..")
from utils.Constantes import Base, COVID_TABLE_NAME, TIME_TABLE_NAME, PROV_TABLE_NAME, TEST_TABLE_NAME


class Covid(Base):
    __tablename__ = COVID_TABLE_NAME

    id = Column(Integer, primary_key=True)
    time_id = Column(Integer, ForeignKey(TIME_TABLE_NAME+".id"), nullable=False)
    test_id = Column(Integer, ForeignKey(TEST_TABLE_NAME+".id"), nullable=False)
    prov_id = Column(Integer, ForeignKey(PROV_TABLE_NAME+".id"), nullable=False)
    dia_inicio = Column(Integer)
    total_casos_confirmados = Column(Integer)
    total_casos_fallecidos = Column(Integer)
    total_casos_recuperados = Column(Integer)
    total_casos_terapia = Column(Integer)
    transmision_tipo = Column(String)

    time = relationship('Tiempo', foreign_keys=[time_id])
    test = relationship('Test', foreign_keys=[test_id])
    prov = relationship('Provincia', foreign_keys=[prov_id])
